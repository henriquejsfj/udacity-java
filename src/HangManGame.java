import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Stream;

public class HangManGame {
    HangMan hangMan;
    HangManGame(){
        hangMan = new HangMan("abacaxi");
    }

    HangManGame(String word){
        hangMan = new HangMan(word);
    }

    HangManGame(String[] wordArray){
        this(wordArray[new Random().nextInt(0, wordArray.length)]);
    }

    HangManGame(File file) throws FileNotFoundException {
        this(new Scanner(file).next().split("^d+$"));
    }

    public static boolean validateInput(String input){
        return input.length() == 1;
    }
    public void loop() {
        while (hangMan.getState() == HangManState.PLAYING){
            Scanner scanner = new Scanner(System.in);
            String input;
            do {
                System.out.println("Give your attempt: ");
                input = scanner.next();
            } while (!validateInput(input));

            hangMan.attempt(input.charAt(0));
            System.out.println(hangMan);
        }
        System.out.println(hangMan.getState());
        System.out.println(hangMan.getWord());
    }
}
