public enum HangManState {

    PLAYING("playing"),
    WIN("win"),
    LOSE("lose");

    private String desc;

    HangManState(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
