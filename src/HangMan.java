import java.util.LinkedList;

public class HangMan {
    private String word;
    private Integer guesses = 0;
    private char[] screen;
    private LinkedList<Character> attempts = new LinkedList<>();
    private Integer limit;

    private HangManState state;

    HangMan(String word, Integer limit){
        this.word = word.toLowerCase().strip();
        this.limit = limit;
        screen = "_".repeat(word.length()).toCharArray();
        state = HangManState.PLAYING;
    }

    HangMan(String word){
        this(word, 10);
    }

    public void attempt(Character letter){
        int ind = word.indexOf(letter);
        if (ind == -1){
            attempts.add(letter);
        }
        while (ind != -1 && screen[ind] == '_') {
            screen[ind] = letter;
            ind = word.indexOf(letter, ind+1);
            guesses++;
        }
        updateState();
    }

    private void updateState(){
        if (attempts.size() >= limit){
            state = HangManState.LOSE;
        } else if (guesses == word.length()) {
            state = HangManState.WIN;
        }
    }

    @Override
    public String toString() {

        return new String(screen) + "\nAttempts: " + attempts + "\nAttempts remaining: " + (limit - attempts.size());
    }

    public HangManState getState() {
        return state;
    }

    public String getWord() {
        return word;
    }
}
